#include <WireKinetis.h>
#include <SPI.h>
#include <TimeLib.h>
#include <Snooze.h>
#include "AudioStream.h"
#include "Audio_reduced.h"
#include "sh1106.h"
#include "Si5338.h"
#include "rx_const.h"
#include "rx_pinout.h"
#include "buttons.h"
#include "hilb.h"

// GUItool: begin automatically generated code
AudioOutputI2S           dac;          //xy=536,115
AudioInputI2S            adc;          //xy=84,123

AudioFilterFIR           hilb_i;           //xy=233,85
AudioEffectDelay         dely_q;         //xy=233,168
AudioMixer4              mixer_l;         //xy=388,115
AudioMixer4              mixer_r;         //xy=388,115

AudioConnection          patchCord1(adc, 0, hilb_i, 0);
AudioConnection          patchCord2(adc, 1, dely_q, 0);

AudioConnection          patchCord3(hilb_i, 0, mixer_l, 0);
AudioConnection          patchCord4(dely_q, 0, mixer_l, 1);
AudioConnection          patchCord5(adc, 0, mixer_l, 2);

AudioConnection          patchCord6(hilb_i, 0, mixer_r, 0);
AudioConnection          patchCord7(dely_q, 0, mixer_r, 1);
AudioConnection          patchCord8(adc, 1, mixer_r, 2);

AudioConnection          patchCord9(mixer_l, 0, dac, 0);
AudioConnection          patchCord10(mixer_r, 0, dac, 1);

AudioAnalyzeCFFT256      fft_2;
AudioConnection          patchCord14(adc, 0, fft_2, 0);
AudioConnection          patchCord15(adc, 1, fft_2, 1);

AudioEffectMagnitude     mag;
AudioConnection          patchCord16(adc, 0, mag, 0);
AudioConnection          patchCord17(adc, 1, mag, 1);
AudioConnection          patchCord18(mag, 0, mixer_l, 3);
AudioConnection          patchCord19(mag, 0, mixer_r, 3);

AudioControlSGTL5000     sgtl5000_1;     //xy=461,211
// GUItool: end automatically generated code

//OUTDIV1 field mask in the SIM_CLKDIV1 register
const uint32_t OD1M = 0xf0000000; 
uint32_t od1 = SIM_CLKDIV1 & OD1M;
uint8_t cpu_x = 0;
const uint8_t CPU_1X = 0;
const uint8_t CPU_2X = 1;

// this is the pin wakeup driver
SnoozeDigital   _digital;
SnoozeBlock config_sleep (_digital);
//SnoozeUSBSerial _usb;
//SnoozeBlock config_sleep (_digital, _usb);

//updated in the encoder ISR
volatile int16_t	enc_cnt = 0;
int16_t enc_rem = enc_cnt;

uint8_t rx_mode = 3; //LSB RX mode
uint8_t lp_mode = 0; //low power disabled
uint8_t fft_en = 1;  //enable FFT display
uint8_t fix_en = 0;  //enable ADC_FIX
uint8_t	csn_en = 0;
uint8_t	grd_en = 0;

//IF AGC
const uint8_t AGC_WT = 100;
uint8_t agc_mode = AGC_M;
uint8_t agc_tmr = AGC_WT;
//updated in the audio library ISR
volatile agc_t agc = AGC_MIN;

//initial AF volume
int16_t	vol = VOL_INI;

float volts = 0;

//analog bandwidth for SCAFs
uint16_t abw = ABW_INI;

int32_t freq = FRQ_INI;
int16_t fstep = FSTEP_INI;

Si5338* Si = NULL;

//used for all sprintf's
static char buf [128];

void set_cpu (uint8_t _cpu_x) {
	cpu_x = _cpu_x;
	//set system/core frequency divider, do not touch bus frequency dividers
  SIM_CLKDIV1 = (SIM_CLKDIV1 & ~OD1M) | (od1>>cpu_x & OD1M);
  //fix millis timer
  //(also micros function in pins_teensy.c needs is hacked)
  SYST_RVR = (F_CPU<<cpu_x)/1000-1;

  Wire.setClock(400000);
}

void set_mix () {
  float gain_i = 1.0;
  float gain_q = 1.0;
  float gain_a = 2.5;
  switch (rx_mode) {
    case 0:
      mixer_l.gain(0, 0);
      mixer_l.gain(1, 0);
      mixer_l.gain(2, gain_i);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, 0);
      mixer_r.gain(1, 0);
      mixer_r.gain(2, gain_q);
      mixer_r.gain(3, 0);
      oled_puts("biq ", 0, 6, 2);
      break;
    case 1:
      mixer_l.gain(0, gain_i);
      mixer_l.gain(1, -gain_q);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, gain_i);
      mixer_r.gain(1, gain_q);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, 0);
      oled_puts("bsb", 0, 6);
      break;
    case 2:
      mixer_l.gain(0, gain_i);
      mixer_l.gain(1, -gain_q);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, gain_i);
      mixer_r.gain(1, -gain_q);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, 0);
      oled_puts("usb", 0, 6);
      break;
    case 3:
      mixer_l.gain(0, gain_i);
      mixer_l.gain(1, gain_q);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, gain_i);
      mixer_r.gain(1, gain_q);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, 0);
      oled_puts("lsb ", 0, 6, 5);
      break;
    case 4:
      mixer_l.gain(0, 0);
      mixer_l.gain(1, 0);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, gain_a);
      mixer_r.gain(0, 0);
      mixer_r.gain(1, 0);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, gain_a);
      oled_puts("am ", 0, 6, 1);
      break;
  }
}

void mute_all (uint8_t l, agc_t _agc = AGC_MIN) {
  sgtl5000_1.volumeInteger(0);
  agc_tmr = max(agc_tmr, l);
  agc_mode = AGC_M;
  agc = _agc;
  analogWrite(AGC_OUT, agc>>DAC_SHR);
}

void set_fstep (uint16_t _fstep) {
  fstep = _fstep;
  
  if (grd_en) {
    sprintf(buf, "G%2uk", fstep/1000);
    oled_puts(buf, 93, 6);
  }
  else {
    sprintf(buf, "%4uHz", fstep);
    oled_puts(buf, 79, 6);
    if (csn_en)
      oled_puts("cs", 85, 6);
  }

  //step changed, need to reinit Si5338 clk0 multysynth
  mute_all (8, agc);
  double f = Si->clk0_set ((double)freq, (double)fstep);
  freq = floor (f + 0.5);
}

time_t getTeensy3Time() {
  return Teensy3Clock.get();
}

void enable_RF() {
  mute_all (AGC_WT);
  fix_en = 1;
  digitalWrite (RF_PWR, HIGH);
  delay (5);
  Si = new Si5338();
  set_fstep (fstep);
}

void disable_RF() {
  mute_all (1);
  digitalWrite (RF_PWR, LOW);
  delete Si;
  Si = NULL;
}

void enable_ENC () {
#if defined(_K6x_)
  //on Teensy 3.5/3.6 there are enough pins to control the encoder power separately
  pinMode (ENC_PWR, OUTPUT);
  digitalWrite (ENC_PWR, HIGH);
#endif
}

void disable_ENC () {
#if defined(_K6x_)
  digitalWrite (ENC_PWR, LOW);
#endif
}

//------------------------------------------------------------------------------
void setup() {
  //init all interfaces and pins
  Wire.begin();
  Wire.setClock(400000);

  //double the CPU/core clock to 48MHz from the 24MHz compile setting
  set_cpu(CPU_2X);

  SPI.setMOSI(SDO);
  SPI.setSCK(SCLK);
  SPI.begin();

  Serial.begin(115200);
  delay(10);
  Serial.println("--- reset ---");

  setSyncProvider(getTeensy3Time);
  if (timeStatus()!= timeSet)
    Serial.println("Unable to sync with the RTC");
  else
    Serial.println("RTC has set the system time");

  //init all button states and set ports to INPUT_PULLUP
	b_init();

  //init display
  oled_init();
  oled_erase ();

  //enable RF POWER
  pinMode (RF_PWR, OUTPUT);
  enable_RF ();

  //encoder inputs
  pinMode (ENC_A, INPUT);   
  pinMode (ENC_B, INPUT);
  attachInterrupt (ENC_A, encoder, CHANGE);
  attachInterrupt (ENC_B, encoder, CHANGE);
  enable_ENC();

  //generate clock for MAX7*
  pinMode (SCAF_CLK, OUTPUT);
  analogWriteFrequency(SCAF_CLK, abw*100);
  analogWrite(SCAF_CLK, 128);

	//used for DAC only or PWM also ?
	analogWriteResolution(12);

  analogReadResolution(13);

  AudioMemory(20);

  hilb_i.begin(hilb, 128); 
  dely_q.delay_tap(0, 64);
  set_mix ();

  /*  
  sampling rate changed to ~16k in the output_i2s.cpp only for 24MHz/48MHz/96MHz (F_PLL=96MHz)
  to avoid horrible MCLK jitter the MCLK_MULT must be 1 or 2
  using 2/47 multiplyer, the sampling rate is ~15.957ksps
  256-bin FFT is ~62.3338Hz
  */
  sgtl5000_1.enable();
  sgtl5000_1.inputSelect(AUDIO_INPUT_LINEIN);
  sgtl5000_1.lineInLevel(0);
	sgtl5000_1.volumeInteger(0);
}

//-------------------------------------------------------------------------------
void loop () {
  static uint32_t	ltime = millis();

	//LOOP_MS base timers/counters
  static uint8_t	lcnt = 0;			//loop counter
	static uint16_t enc_tmr = 0;	//last encoder event
	static uint16_t lbe_tmr = 0;	//last button event

	//*_d display (or delayed) values
  //used for "single-shot" display updates etc.
  static int32_t 	freq_d = 0;
  static uint16_t	abw_d = 0;
  static uint16_t vol_d = vol;
  static agc_t 		agc_d = 0;
	static uint8_t  agc_mode_d = ~0;
	static uint8_t 	sec_d = 0;

	//when carrier snap is enabled encoder arms snap timer
  static uint8_t	enc_csn_arm = 0;
  static uint8_t	enc_csn_arm_d = 0;

	//timing stats for debug
  static uint32_t	ms_used = 0;
  static uint32_t wk_cnt = 0;

	//period corrected loop
  ltime += LOOP_MS;
  while (ltime > millis()) {
    asm("WFI");
    yield ();
    wk_cnt++;
  }
  //update timers
  enc_tmr++;
  lbe_tmr++;
  lcnt++;

  if (b_lock & LPE_LOCK)
    enc_cnt = 0, enc_rem = 0;
  
  //update the array of buttons
  int b_cnt = b_update();
  if (b_cnt) lbe_tmr = 0;
  
  //power-off (hibernate) enter-exit on button 0
  //modal operation 
  if (!lp_mode and !(b_lock & ENC_LOCK) and b_p(0, 3*1000/LOOP_MS)) {
    lp_mode = 2;
    oled_erase (4, 5);
    sprintf (buf, "PWR OFF");
    oled_puts(buf, 35, 4);
    return;
  }

  if (lp_mode == 2) {
    //wait after b0 release to avoid immediate interrupt
    if (!b_r(0,30)) return;

    //entering standby mode
    disable_ENC ();
    disable_RF ();
    oled_erase (4, 5);
    oled_sleep ();
//    Serial.end();
    SPI.end ();
//    Wire.end();
    sgtl5000_1.disable(); //modified, writes 0 to CHIP_ANA_POWER
    set_cpu (CPU_1X);

    _digital.pinMode (B_PINS[0], INPUT_PULLUP, FALLING);
//    Snooze.deepSleep (config_sleep); //sleeps here until b0 interrupt
    Snooze.hibernate (config_sleep); //sleeps here until b0 interrupt
    pinMode (B_PINS[0], INPUT_PULLUP);

    set_cpu (CPU_2X);
//    Wire.begin();
    SPI.begin ();
//    Serial.begin(115200);
    oled_awake ();
    enable_RF ();
    enable_ENC ();
    sgtl5000_1.enable();
    sgtl5000_1.volumeInteger(0);
    sgtl5000_1.inputSelect(AUDIO_INPUT_LINEIN);
    sgtl5000_1.lineInLevel(0);

    setSyncProvider(getTeensy3Time);
    b_init();
    b_lock = LPE_LOCK;
    lp_mode = 0;
    return;
  }

  if (!fft_en) {
    sprintf (buf, "LOW SIG");
    if (adc.corrtmr() == C_END) fft_en = 1;
    else if (adc.corrtmr() > C_END-500) oled_puts(buf, 35, 4);
  }
  else if (adc.corrtmr() < C_END and adc.corrtmr() > C_START) {
    fft_en = 0;
    oled_erase (4, 5);
    sprintf (buf, "ADC FIX");
    oled_puts(buf, 35, 4);
  }

	//hack to wait out startup AF transient thump
	if (agc_tmr and --agc_tmr == 0) {
		agc_mode = AGC_F;
		sgtl5000_1.volumeInteger(vol);
    if (fix_en) fix_en = 0, adc.correct();
	}

  //process button actions on short b_s and long b_l release events
	//a bit messy
	if (!b_cnt and !lp_mode and !b_lock) {
    const uint16_t LP_DLY = 15*60*1000/LOOP_MS;
    //b0----
		if (b_s(0)) {
      //cycle thru rx modes
			if (csn_en) {
				if (++rx_mode > 3) rx_mode = 1;
			}
			else {
				if (++rx_mode > 4) rx_mode = 1;
			}
			set_mix();
		}
    else if (b_l(0) or (enc_tmr > LP_DLY and lbe_tmr > LP_DLY)) {
      lp_mode = 1;
      disable_ENC ();
      oled_erase (4, 5);
      sprintf (buf, "LP mode");
      oled_puts(buf, 35, 4);
      set_cpu (CPU_1X);
    }
    //b1----
		else if (b_s(1)) {
      //AGC ON, cycle tru AGC modes
			if (!agc_mode)
				//manual gain -> fast AGC
				agc_mode = AGC_F;	
			else
				//toggle fast/slow
				agc_mode ^= AGC_F^AGC_S;
		}
    //b2----
		else if (b_s(2)) {
      //cycle thru frequency steps
      if (fstep == 1) {
        csn_en = 0;
        enc_csn_arm = 0;
        enc_csn_arm_d = 0;
        set_fstep (FSTEP_INI);
      }
      else if (fstep == FSTEP_INI) {
        grd_en = 1;
        if (freq_d > FRQ_MW)
          set_fstep (FSTEP_GSW);
        else
          set_fstep (FSTEP_GMW);
      }
      else {
        grd_en = 0;
        set_fstep (1);
      }
    }
    else if (b_l(2)) {
      if (rx_mode != 4) {
        //toggle carrier snap
        csn_en ^= 1;
        if (csn_en and fstep != 1) {
          grd_en = 0;
          set_fstep (1);
        }
      }
      else
        csn_en = 0;
      enc_csn_arm = csn_en;

      if (csn_en)
        oled_puts("cs", 85, 6);
      else
        oled_puts("  ", 85, 6);
    }
	}

  //snap to carrier action 
  if (enc_csn_arm and fft_2.max_cnt == 0 and enc_tmr > 50) {
    enc_csn_arm = 0;
    freq += (int32_t)(fft_2.freq_est);
    //too many steps may be required, lets click
    set_fstep(1);
  }
  static uint8_t	rx_mode_p = 0;
	if (enc_csn_arm == 1 and enc_csn_arm_d == 0) {
		//temporarily switch to AM mode during snap search
		rx_mode_p = rx_mode;
		rx_mode = 4;
		set_mix ();
	}
	else if (enc_csn_arm == 0 and enc_csn_arm_d == 1) {
		//return to the previous mode
		rx_mode = rx_mode_p;
		set_mix ();
	}
  enc_csn_arm_d = enc_csn_arm;

  //process encoder
  int32_t steps = 0;
  if (enc_cnt and !lp_mode) {
    //quadratic acceleration
    enc_cnt *= abs(enc_cnt);
    enc_cnt += enc_rem;
    int8_t slow = ENC_PPR;
    if (grd_en) slow += 1;
    steps = enc_cnt >> slow;
    enc_rem = enc_cnt-(steps<<slow);
    enc_cnt = 0;
    enc_tmr = 0;

    //add one step hysteresis
    static int32_t psteps = 0;
    if (steps*psteps == -1) psteps = steps, steps = 0;
    else if (steps) psteps = steps;

    //all buttons released - frequency control
    if (!b_cnt and steps) {
      //limits due to synth I2C speed
      steps = min(STEPS_MAX, max(-STEPS_MAX, steps));
      //update frequency
      freq += steps*fstep;
      freq = min(FRQ_MAX, max(FRQ_MIN, freq));
      enc_csn_arm = csn_en;
    }

    //button is pressed - alternative controls
    if (b_cnt == 1 and steps) {
      if (b_p(0)) {
        vol += steps;
        vol = min(max(vol, VOL_MIN), VOL_MAX);
        sgtl5000_1.volumeInteger(vol);
      }
      else if (b_p(1)) {
        agc += steps<<1<<DAC_SHR;
        agc = min(max(agc, AGC_MIN), AGC_MAX);
        agc_mode = 0;
      }
      else if (b_p(2)) {
        abw += steps*20;
        abw = min(max(abw, ABW_MIN), ABW_MAX);
      }
      //lock long press action, encoder has moved
      b_lock = ENC_LOCK;
    }
  }

  //exit LP mode on any button press
  if (lp_mode == 1 and b_cnt) {
    set_cpu (CPU_2X);
    enable_ENC ();
    oled_erase (4, 5);
    abw_d = 0;  //force AF bw display update
    enc_rem = 0;
    lp_mode = 0;
    //lock long press action, exiting LP mode
    b_lock = LPE_LOCK;
  }

  if (grd_en) {
    //switch fstep when crossing the upper limit of the MW band
    if (freq_d > FRQ_MW and freq <= FRQ_MW)
      set_fstep(FSTEP_GMW);
    else if (freq_d < FRQ_MW and freq >= FRQ_MW)
      set_fstep(FSTEP_GSW);
  }
  
  //HW and display updates on single-shots
  if (freq != freq_d) {
    freq_d = freq;
    Si->fsteps (steps);
    //display freq
    sprintf (buf, "%6lu.%03lu kHz", (freq-FRQ_IF)/1000, (freq-FRQ_IF)%1000);
    oled_puts(buf, 0, 0);
  }
  
	if (agc_mode != agc_mode_d){
		agc_mode_d = agc_mode;
		oled_putc (' ', 40, 6);
		if (!agc_mode)
			oled_putc ('M', 40, 6);
		else if (agc_mode == AGC_S)
			oled_putc ('S', 44, 6);
		else
			oled_putc ('F', 44, 6);
	}

  if (agc != agc_d) {
    agc_d = agc;
    analogWrite(AGC_OUT, agc>>DAC_SHR);
    //display agc value
    sprintf (buf, "%3lu", (uint32_t)agc>>AGC_SHL);
    oled_puts(buf, 54, 6);
  }

  static auto b0p_d = b_p(0);
  if (b0p_d != b_p(0)) {
    b0p_d = b_p(0);
    if (b0p_d) vol_d = 0;
    oled_erase (2,3);
  }

  if (b_p(0) and vol != vol_d) {
    vol_d = vol;
    //display volume
    sprintf (buf, "vol %4u", vol);
    oled_puts(buf, 42, 2);
  }
  
	static auto b2p_d = b_p(2);
	if (b2p_d != b_p(2)) {
		b2p_d = b_p(2);
		if (b2p_d) abw_d = 0;
		oled_erase (2,3);
	}

  if (b_p(2) and abw != abw_d) {
    abw_d = abw;
    analogWriteFrequency(SCAF_CLK, abw*100);
    //display audio bandwidth
    sprintf (buf, "%4u  Hz", abw);
    oled_puts(buf, 42, 2);
  }

	if (!b_cnt and sec_d != second()) {
    sec_d = second();
	  //display time and date
		time_t t = now();
    sprintf (buf, "%02d : %02d : %02d", hour(t), minute(t), second(t));
    oled_puts(buf, 15, 2, 1);
    /*
    sprintf (buf, "%02d:%02d:%02d", hour(t), minute(t), second(t));
    oled_puts(buf, 6, 2);
    const char* wd = " UMTWRFS";
    sprintf (buf, "%c %02d/%02d ", wd[weekday(t)], month(t), day(t));
    oled_puts(buf, 72, 2, 4);
    */

    volts = 13.25*analogRead(A7)/(1<<14);
    if (volts > 4.1 or sec_d%2) {
      sprintf (buf, "%.2fv", volts);
      oled_puts(buf, 82, 2);
    }
    else
      oled_puts(" .  ", 82, 2);

		//debug
		if (!lp_mode and Serial)
			oled_putc('^', 122, 0);
		else
			oled_putc(' ', 122, 0, 6);
  }

  if(lcnt%2 == 0 and !lp_mode and fft_en) {
    //update FFT graph
    uint8_t r1[128], r2[128];
    for (int i = 0; i < 128; i++) {
      int16_t ix = 192-i;
      uint16_t dot = 1<<(fft_2.l2mag[ix]>>4);
      if (ix == fft_2.max_i and enc_csn_arm) dot ^= 0xffff;
      r1[i] = dot>>8;
      r2[i] = dot;
    }
    oled_addr (0, 4);
    oled_data (r1, 128);
    oled_addr (0, 5);
    oled_data (r2, 128);
  }

	if (!lp_mode and Serial)
	{
		//listen for time updates from the "Porcessing" IDE port emulator
		if (Serial.available() and Serial.find("T")) {
			time_t t = Serial.parseInt();
			if (t) Teensy3Clock.set(t), setTime(t);
		}

//    sprintf (buf, "%1d %1d %1d", csn_en, enc_csn_arm, enc_csn_arm_d);
//    Serial.println(buf);

#if (1)
	  //various debug reporting
	  void debug2serial(char*);
	  const uint8_t REPT = 64;
    static uint32_t enc_cnt_avr = 0;
    enc_cnt_avr += abs(enc_cnt);
	  if (lcnt%REPT == 0) {
	      sprintf (buf, "LTU %.2fms WKC %.2f ENC %.2f", (float)ms_used/REPT, (float)wk_cnt/REPT, (float)enc_cnt_avr/REPT);
	      wk_cnt = 0;
				ms_used = 0;
        enc_cnt_avr = 0;
	      debug2serial (buf);
	  }
#endif
	}

  ms_used += millis()-ltime;
}

//ISR function ------------------------------------------------
void encoder ()
{
  static uint8_t pstate = 0;

  //process quadrature state at any speed
  uint8_t state = digitalRead(ENC_A);
  if (digitalRead(ENC_B)) state ^= 3;
  if (state == ((pstate + 1)&3))
    enc_cnt++;
  if (state == ((pstate - 1)&3))
    enc_cnt--;
  pstate = state;
}


//debug info to Serial ----------------------------------------
void debug2serial(char* buf) {
#if defined(__MK20DX256__)
    Serial.println("---T3.2---");
#elif defined(__MK64FX512__)
    Serial.println("---T3.5---");
#elif defined(__MK66FX1M0__)
    Serial.println("---T3.6---");
#endif

#if(1)
    Serial.println (buf);
#endif
   
#if(0)
    sprintf (buf, "F_PLL %uM, F_BUS %uM", F_PLL/1000000, F_BUS/1000000);
    Serial.println(buf);
#endif    

#if(1)
    sprintf (buf, "F_CPU compiled %uM, F_CPU current %uM", F_CPU/1000000, (F_CPU/1000000)<<cpu_x);
    Serial.println(buf);
#endif

#if(1)
    sprintf (buf, "AUDIO_SAMPLE_RATE %.3fksps", AUDIO_SAMPLE_RATE/1000);
    Serial.println(buf);
    sprintf (buf, "AudioP %.2f%%", AudioProcessorUsageMax());
    AudioProcessorUsageMaxReset();
    Serial.println(buf);
    sprintf (buf, "AudioM %d", AudioMemoryUsageMax());
    AudioMemoryUsageMaxReset();
    Serial.println(buf);
#endif  

#if (0)
    Serial.print ("CLKDIV1 ");
    Serial.println (SIM_CLKDIV1, HEX);
    Serial.print ("SYST_RVR ");
    Serial.println (SYST_RVR, HEX);
#endif

#if(0)
    Serial.println (SIM_SCGC1, HEX);
    Serial.println (SIM_SCGC2, HEX);
    Serial.println (SIM_SCGC3, HEX);
    Serial.println (SIM_SCGC4, HEX);
    Serial.println (SIM_SCGC5, HEX);
    Serial.println (SIM_SCGC6, HEX);
    Serial.println (SIM_SCGC7, HEX);
#endif

#if(1)
		sprintf(buf, "agc_mode %u, agc %u", agc_mode, (uint16_t)(agc>>DAC_SHR));
		Serial.println(buf);
#endif

#if(0)    
    if (fft_2.max_cnt == 0) {
      Serial.println(fft_2.freq_est);
    }
#endif    

#if(1)
    sprintf (buf, "volts %.2f", volts);
    Serial.println (buf);
#endif

#if(0)
    extern volatile float cpd01;
    extern volatile float cpd00;
    extern volatile float cpd10;
    extern volatile int16_t c_tmr;
    extern volatile int8_t c_sel;
    sprintf (buf, "sel %d, tmr %d, %.2e, %.2e, %.2e", c_sel, c_tmr, cpd01, cpd00, cpd10);
    Serial.println (buf);
#endif
}

