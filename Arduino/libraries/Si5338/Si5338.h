#include <Arduino.h>
#include <WireKinetis.h>

//RX board parameters
const double Ftcxo = 25e6L;
const double Fif = 45e6L;
const double Flo = Fif + 30e3;
const double Fhi = Fif + 30e6;

//Si5338 VCO parameters
const double VCOmin = 2.2e9L;
const double VCOmax = 2.84e9L;
const double Fpfd = Ftcxo;
const uint8_t Nclk = 4;

//Si5338 I2C address
const uint8_t si_addr = 0x70;

//skipping all synth inp/out/VCO setup
//for constant parameters will use ClockBuilder
//generated register_map.h for now
class Si5338 {
    void s_reset();
    uint8_t b_read(uint8_t adr);
    void b_write(uint8_t adr, uint8_t v);
    void b_rmw(uint8_t adr, uint8_t v, uint8_t msk);
    
  public:
    //loads ClockBuilder defaults to the Si5338 RAM
    Si5338();

    //returns corrected Fout (hopefully)
    double clk0_set (double Fout, double Fstep);
    //returns clk0 frequency
    uint8_t fsteps(int8_t steps);
};

