#include "F2F.h"
#include <stdio.h>

void F2F::print() {
  char buf[128];
  sprintf (buf, "frac: %lu %lu/%lu, v: %.4e, err: %.4e", i, n, d, v, err);
  Serial.println (buf);
}

//Stern-Brocot tree search on the interval [0/1,1/1]
//speed greatly improved using idea from:
//"Approximating Rational Numbers by Fractions" by M.Forisek
F2F::F2F (double _v, double prec, int32_t d_max) {
  v = _v;
  i = floor(v);
  double f = v-i;
  uint32_t xn = 0, xd = 1, yn = 1, yd = 1;   //initial search interval 0/1..1/1
  char buf[128];

  for(uint32_t j=1;;j++) {
    //calculate the number of steps in one directions until error sign changes
    uint32_t k = ceil((yn - f*yd)/(f*xd - xn));
    d_max -= k*xd;
    if (d_max < 0)  //do not update x's
        k += d_max/xd,
        yn += k*xn, yd += k*xd;
    else
        yn += k*xn, yd += k*xd,
        xn = yn - xn, xd = yd - xd;

    double xerr = (double)xn/xd - f;
    double yerr = (double)yn/yd - f;

    if (d_max < 0) {
      sprintf (buf, "return on exceeding d_max, N iterations %lu", j);
      Serial.println (buf);
      if (abs (yerr) < abs (xerr))
        n = yn, d = yd, err = yerr;
      else
        n = xn, d = xd, err = xerr;
      return;
    }

    //xd < yd always, so if we pass prec check - return
    if (abs (xerr) <= prec) {
      sprintf (buf, "return on passed prec test, N iterations %lu", j);
      Serial.println (buf);
      n = xn, d = xd, err = xerr;
      return;
    }

    if (abs (yerr) <= prec) {
      sprintf (buf, "return on passed prec test, N iterations %lu", j);
      Serial.println (buf);
      n = yn, d = yd, err = yerr;
      return;
    }
  }
}

/*
//approximation uses Stern-Brocot tree search on the interval [0/1,1/1]
F2F::F2F (double _v, double prec, int32_t d_max) {
  v = _v;
  i = floor(v);
  double tf, f = v-i;
  uint32_t ln = 0, ld = 1, rn = 1, rd = 1;
  char buf[256];

  for (uint32_t k=0;;k++) {
    //Farey sequence is not uniformly distributed on [0.0,1.0]
    //needs a hack to speed-up searching corner regions
    if (ld == 1 && (double)rn/(2*rd)-f > 0)
      //left corner binary search
      n = rn, d = 2*rd;
    else if (rd == 1 && f-(double)(2*ln+1)/(2*ld) > 0)
      //right corner binary search
      n = 2*ln+1, d = 2*ld;
    else
      //find mediant
      n = ln+rn, d = ld+rd;

    if (d > d_max) {
      sprintf (buf, "return on nom or den exceeding max, N iterations %ld", k);
      Serial.println(buf);
			double lerr = (double)ln/ld - f;
			double rerr = (double)rn/rd - f;
			if (abs(lerr) < abs(rerr))
				n = ln, d = ld, err = lerr;
			else
				n = rn, d = rd, err = rerr;
			return;
    }

    tf = (double)n/d;
    err = tf-f;
    if (err > prec)
      rn = n, rd = d;
    else if (-err > prec)
      ln = n, ld = d;
    else {
      sprintf (buf, "return on passed prec test, N iterations %ld", k);
      Serial.println(buf);
      return;
    }      
  }
}
*/


