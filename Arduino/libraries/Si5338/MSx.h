#include <Arduino.h>
#include "F2F.h"

class MSx {
  public:
    F2F fr;
    double Fout;
    uint32_t P1, P2, P3;
    uint64_t FIDP1, FIDP2, FIDP3;
    MSx(double Fvco, double _Fout, double Fstep);
    void print ();    
};

