#include "MSx.h"

//assuming R[0..3] dividers are equal to 1
//for the project I do not need them
//small Fstep requires some special conditions (see RM 6.2)
MSx::MSx(double Fvco, double _Fout, double Fstep) {
        //correct either Fstep or Fout for an integer ratio
        uint32_t r = floor(_Fout/Fstep + 0.5);
        Fout = Fstep*r;
        Serial.println("corrected Fout ");
        Serial.println(Fout, 4);

        //convert float to the fractional format
        fr = F2F(Fvco/Fout, 1e-12L, 1<<22);
				uint32_t a = fr.i, b = fr.n, c = fr.d;

        //Fout parameters
        uint64_t x = (uint64_t)a*c+b;
        P1 = (int32_t) floor(x*128.0L/c - 512);
        P2 = (b*128)%c; //b,c less tan 22bit, keep 32 bits
        P3 = c;

        //Fstep parameters
        FIDP1 = (uint64_t)c*r;
        FIDP2 = c;
        FIDP3 = x*r;
}

void MSx::print() {
  char buf[256];
  fr.print();
  sprintf(buf, "P1 %lx, P2 %lx, P3 %lx\n", P1, P2, P3);
  Serial.print(buf);
  sprintf(buf, "FIDP1 %llx, FIDP2 %llx, FIDP3 %llx\n", FIDP1, FIDP2, FIDP3);
  Serial.print(buf);
  
}


