#include "Si5338.h"
#include "register_map.h"
#include "MSx.h"

//I2C debug printout
void i2c_print(char* s, uint8_t adr, uint8_t val) {
  Serial.print(s);
  Serial.print(adr);
  Serial.print(":\t");
  Serial.println(val, HEX);
}

void i2c_print(char* s, uint8_t adr, uint8_t val, uint8_t msk) {
  Serial.print(s);
  Serial.print (adr);
  Serial.print (":\t");
  Serial.print (val, HEX);
  Serial.print (",\t");
  Serial.println (msk, HEX);
}

//write file from the entire ClockBuilder register_map.h to the chip RAM
Si5338::Si5338() {
  uint8_t val, msk, adr;

  //disable outputs
  b_rmw (230, 1<<4, 1<<4);

  //pause LOL
  b_rmw (241, 1<<7, 1<<7);

  for (int16_t i = 0; i < NUM_REGS_MAX; i++) {
    adr = Reg_Store[i].Reg_Addr;
    val = Reg_Store[i].Reg_Val;
    msk = Reg_Store[i].Reg_Mask; 
    if (msk == 0)
      continue;
    else if (msk == 0xff)
      b_write (adr, val);
    else
      b_rmw (adr, val, msk);
  }

  //check input clock
  if (b_read(218)&(1<<2)) {
    Serial.println("input clock not present");
    Serial.println("exiting...");
    return;
  };

  //configure PLL for locking
  b_rmw(49, 0, 1<<7);

  //initiate PLL locking
  s_reset();
  delay(25);

  //restart LOL
  b_rmw (241, 0, 1<<7);
  b_write (241, 0x65);

  //check PLL lock
  if (b_read(218)&(1<<4)) {
    Serial.println("PLL_LOL");
    Serial.println(", exiting...");
    return;
  } 

  //copy FCAL values to active registers
  val = b_read (237);
  b_rmw (47, val, 0x3);
  val = b_read (236);
  b_write (46, val);
  val = b_read (235);
  b_write (45, val);
  b_rmw (47, 5<<2, 0xfc);

  //set PLL to use FCAL values
  b_rmw(49, 1<<7, 1<<7);

  //spread spectrum not used
  //...

  //enable outputs
  b_rmw (230, 0, 1<<4);

  Serial.println("exit Si5338 constructor");
}

void Si5338::s_reset() {
  b_write (246, 1<<1);
//  delay(10);
//  b_write (246, 0);
}

void Si5338::b_write(uint8_t adr, uint8_t val) {
  Wire.beginTransmission(si_addr);
  Wire.write(adr);
  Wire.write(val);
//  i2c_print("w1\t", adr, val);
  uint8_t ete = Wire.endTransmission(1);
  if (ete) {
    Serial.print("I2C w err:\t");
    Serial.println(ete);
  }
}

uint8_t Si5338::b_read(uint8_t adr) {
  Wire.beginTransmission(si_addr);
  Wire.write(adr);
  Wire.endTransmission();
  uint8_t ete = Wire.endTransmission(1);
  if (ete) {
    Serial.print("I2C w err:\t");
    Serial.println(ete);
  }

  Wire.requestFrom (si_addr, (uint8_t)1, (uint8_t)1);
  while (Wire.available() == 0) {
    Serial.print("-.");
    delay(100);
  }
  uint8_t val = Wire.read();
//  i2c_print("r1\t", adr, val);
  
  return val;
}

void Si5338::b_rmw(uint8_t adr, uint8_t v, uint8_t msk) {
  uint8_t rbk = b_read(adr);
  uint8_t val = (rbk & ~msk) | (v & msk);
  b_write(adr, val);
}

//returns corrected Fout (hopefully)
double Si5338::clk0_set (double Fout, double Fstep) {
  MSx MSx0(2.7e9L, Fout, Fstep);  
  MSx0.print();

  //write frequency registers (byte boundaries not aligned)
  b_write (53, uint8_t(MSx0.P1));
  b_write (54, uint8_t(MSx0.P1>>8));
  b_write (55, uint8_t((MSx0.P1>>16)&0x3) | uint8_t((MSx0.P2<<2)&0xFC));
  b_write (56, uint8_t(MSx0.P2>>6));
  b_write (57, uint8_t(MSx0.P2>>14));
  b_write (58, uint8_t(MSx0.P2>>22));
  b_write (59, uint8_t(MSx0.P3));
  b_write (60, uint8_t(MSx0.P3>>8));
  b_write (61, uint8_t(MSx0.P3>>16));
  b_rmw (62, uint8_t(MSx0.P3>>24), 0x3F);

  //write frequency step registers
  b_write (123, uint8_t(MSx0.FIDP1));
  b_write (124, uint8_t(MSx0.FIDP1>>8));
  b_write (125, uint8_t(MSx0.FIDP1>>16));
  b_write (126, uint8_t(MSx0.FIDP1>>24));
  b_write (127, uint8_t(MSx0.FIDP1>>32));
  b_write (128, uint8_t(MSx0.FIDP1>>40));
  b_rmw   (129, uint8_t(MSx0.FIDP1>>48), 0x0F);

  b_rmw   (130, uint8_t(MSx0.FIDP2>>48), 0x0F);
  b_write (131, uint8_t(MSx0.FIDP2>>40));
  b_write (132, uint8_t(MSx0.FIDP2>>32));
  b_write (133, uint8_t(MSx0.FIDP2>>24));
  b_write (134, uint8_t(MSx0.FIDP2>>16));
  b_write (135, uint8_t(MSx0.FIDP2>>8));
  b_write (136, uint8_t(MSx0.FIDP2));
  
  b_write (137, uint8_t(MSx0.FIDP3));
  b_write (138, uint8_t(MSx0.FIDP3>>8));
  b_write (139, uint8_t(MSx0.FIDP3>>16));
  b_write (140, uint8_t(MSx0.FIDP3>>24));
  b_write (141, uint8_t(MSx0.FIDP3>>32));
  b_write (142, uint8_t(MSx0.FIDP3>>40));
  b_write (143, uint8_t(MSx0.FIDP3>>48));
  b_write (144, uint8_t(MSx0.FIDP3>>56)&0x7F );

//  s_reset();
  b_write (226, 0x4);
  b_write (226, 0x0);

  return MSx0.Fout;
}

//returns clk0 frequency
uint8_t Si5338::fsteps(int8_t steps) {

  if (steps == 0)
    return 0;
  uint8_t rbk = b_read(52);
  uint8_t msk = 0x60;
  uint8_t dir = 2<<5;
  if (steps < 0)
    dir = 3<<5, steps = -steps;
  uint8_t val = (rbk & ~msk) | (dir & msk);
  for (int8_t i = 0; i < steps; i++)
    b_write(52, val);

  return steps;
}

  


