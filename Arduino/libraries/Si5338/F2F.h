#include <Arduino.h>

class F2F {
  public:
    uint32_t i,n,d;
    double v;
    double err;
    F2F(uint32_t _i = 0L, uint32_t _n = 0L, uint32_t _d = 1L) {
      i = _i, n = _n, d = _d;
      v = (double)i + (double)n/d, err = 0.0L;
    }
    F2F(double _v, double prec, int32_t d_max);
    
    void print();
};

