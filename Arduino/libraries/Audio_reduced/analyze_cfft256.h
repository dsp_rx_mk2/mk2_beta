#ifndef analyze_cfft256_h_
#define analyze_cfft256_h_
#include "Arduino.h"
#include "AudioStream.h"
#include "utility/dspinst.h"
#include "arm_math.h"

extern "C" {
extern const int16_t AudioWindowHanning256[];
extern const int16_t AudioWindowBartlett256[];
extern const int16_t AudioWindowBlackman256[];
extern const int16_t AudioWindowFlattop256[];
extern const int16_t AudioWindowBlackmanHarris256[];
extern const int16_t AudioWindowNuttall256[];
extern const int16_t AudioWindowBlackmanNuttall256[];
extern const int16_t AudioWindowWelch256[];
extern const int16_t AudioWindowHamming256[];
extern const int16_t AudioWindowCosine256[];
extern const int16_t AudioWindowTukey256[];
}

#define MA_LEN 10

class AudioAnalyzeCFFT256 : public AudioStream
{
public:
	AudioAnalyzeCFFT256() : AudioStream(2, inputQueueArray),
	window(AudioWindowHanning256), prevblocka(NULL), prevblockb(NULL) {
		for (int i = 0; i < MA_LEN; i++) mqe_ma[i] = 0;
		ma_p = 0, mqe_sum = 0.;
		arm_cfft_radix4_init_q15(&fft_inst, 256, 0, 1);
	}
	virtual void update(void);
	uint8_t l2mag[256];
	int16_t max_i;
	int16_t max_cnt;
	float freq_est;
	
private:
	int16_t max_l, max_r;
	int16_t ma_p;
	float mqe_ma[MA_LEN];
	float mqe_sum;
	const int16_t *window;
	audio_block_t *prevblocka;
	audio_block_t *prevblockb;
	int16_t buf[512] __attribute__ ((aligned (4)));
	audio_block_t *inputQueueArray[2];
	arm_cfft_radix4_instance_q15 fft_inst;
};

#endif
