#include "analyze_cfft256.h"

void copy_interleave_block (int16_t* dst, const int16_t* src_a, const int16_t* src_b)
{
	for (int i = 0; i < AUDIO_BLOCK_SAMPLES; i++) {
		*dst++ = *src_a++;
		*dst++ = *src_b++;
	}
}

//Jacobsen MQE interpolator
float mqe (int16_t* buf, int16_t n)
{
	int16_t l = 2*((n-1)^0x80);
	int16_t c = 2*((n)^0x80);
	int16_t r = 2*((n+1)^0x80);

	float nom_re = buf[r]-buf[l];
	float nom_im = buf[r+1]-buf[l+1];
	float den_re = 2*buf[c]+buf[r]+buf[l];
	float den_im = 2*buf[c+1]+buf[r+1]+buf[l+1];
	float delta = (nom_re*den_re+nom_im*den_im)/(den_re*den_re+den_im*den_im);
	//bin size for exact 16ksps is 62.5Hz
	//bin size for actual I2S master clock dividers ~62.3338
	const float hz_per_bin = 96e6*2/47/8/32/256;
	return ((128-n)+0.55*delta)*hz_per_bin; 
}

void AudioAnalyzeCFFT256::update(void)
{
	audio_block_t *blocka, *blockb;

	blocka = receiveReadOnly(0);
	blockb = receiveReadOnly(1);

	extern uint8_t lp_mode;
	if (lp_mode) {
		if (blocka) release(blocka);
		if (blockb) release(blockb);
		max_cnt = MA_LEN;
		return;
	}

	if (!blocka) {
		if (blockb) release(blockb);
		return;
	}
	if (!blockb) {
		release(blocka);
		return;
	}
	if (!prevblocka || !prevblockb) {
		prevblocka = blocka;
		prevblockb = blockb;
		return;
	}

	//copy interleaving re/im data
	copy_interleave_block (buf, prevblocka->data, prevblockb->data);
	copy_interleave_block (buf+256, blocka->data, blockb->data);

	//apply window
	for (int i = 0; window && i < 512; i++)
		buf[i] = ((int32_t)buf[i]*window[i>>1])>>15;
	
	//call CMSIS fft
	arm_cfft_radix4_q15 (&fft_inst, buf);

	//calculate 2xlog2 of the magnitude
	//save peak bin number
	int16_t _max_i = -1;
	uint32_t _max_v = 0;
	for (int i = 0; i < 256; i++) {
		int16_t v = buf[2*i];
		uint32_t mag = (int32_t)v*v;
		v = buf[2*i+1];
		mag+= (int32_t)v*v;
//		l2mag[i^0x80] = 32-count_leading_zeros (mag);
		l2mag[i^0x80] += 32-count_leading_zeros (mag) - (l2mag[i^0x80]>>3);
		if (mag > _max_v) {
			_max_v = mag;
			_max_i = i^0x80;
		}
	}

	float freq = mqe (buf, _max_i);
	if (abs(freq-mqe_sum) < 10) {
		if (max_cnt > 0) max_cnt--;
		else freq_est = mqe_sum;
	}
	else {
		max_cnt = MA_LEN;
	}
	max_i = _max_i;

	//moving average filter for the MQE output
	mqe_sum -= mqe_ma[ma_p];
	mqe_ma[ma_p] = freq/MA_LEN;
	mqe_sum += mqe_ma[ma_p];
	if (++ma_p >= MA_LEN) ma_p = 0;


//	if (_max_i > 0 && _max_i < 255)
//		freq_est = mqe (buf, _max_i);

	release(prevblocka);
	release(prevblockb);
	prevblocka = blocka;
	prevblockb = blockb;
}

