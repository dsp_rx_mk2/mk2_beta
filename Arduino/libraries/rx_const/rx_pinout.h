#ifndef _rx_pinout_h_
#define _rx_pinout_h_

#define _V_094_	1

const int OLED_RST	= 2;
const int OLED_DC		= 1;
const int OLED_CS		= 0;

const int SDO		= 7;
const int SCLK	= 14;

//list of pushbutton pins
#if (_V_094_)
	const uint8_t B_PINS[] = {4, 5, 20};
#else
	const uint8_t B_PINS[] = {21, 4, 20};
#endif
const uint8_t N_B = sizeof(B_PINS)/sizeof(B_PINS[0]);

const int ENC_A			= 16;
const int ENC_B			= 17;

const int SCAF_CLK	= 3;
const int RF_PWR		= 8;

#if defined(__MK20DX256__)
const int AGC_OUT 	= A14;
#elif defined(__MK64FX512__) or defined(__MK66FX1M0__)
#define _K6x_
const int AGC_OUT 	= A22;
//additional functions for k6x
const int ENC_PWR		= 27;
const int AGC_IN		= A7;
#endif

#endif