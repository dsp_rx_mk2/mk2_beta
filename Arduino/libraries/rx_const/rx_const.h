#ifndef _rx_const_h_
#define _rx_const_h_

const int32_t FRQ_IF 	= 45e6;
const int32_t FRQ_INI	= 45e6+7e6;
const int32_t FRQ_MW 	= 46805000;
const int32_t FRQ_MAX = 75000000;
const int32_t FRQ_MIN = 45010000;

//const uint8_t ENC_PPR 	= uint8_t(128/32); 
const uint8_t ENC_PPR 	= uint8_t(256/32); 
const int16_t FSTEP_INI	= 50;
const int16_t FSTEP_GSW	= 5000;
const int16_t FSTEP_GMW	= 10000;

typedef uint32_t agc_t;
//AGC bit precision: agc var, display, DAC
const uint8_t AGC_DIS	= 8;	//display range in bits
const uint8_t AGC_SHL	= 14;	//extending resolution
const uint8_t DAC_RES	= 12;
const uint8_t DAC_SHR	= AGC_DIS+AGC_SHL-DAC_RES; 
//AGC sample target thresholds in bits
const uint8_t AGC_PEAK	= 11;
const uint8_t AGC_TRGT	= 8;
const uint8_t AGC_PSHL	= AGC_SHL-4;
//AGC modes
const uint8_t AGC_M		= 0;
const uint8_t AGC_S		= 1;
const uint8_t AGC_F		= 3;
//AGC variable value limits
const agc_t AGC_MIN		= 50L<<AGC_SHL;
const agc_t AGC_MAX		= 200L<<AGC_SHL;

const uint16_t ABW_MIN	= 1000;
const uint16_t ABW_MAX	= 6000;
const uint16_t ABW_INI	= 3800;
const uint16_t ABW_STP	= 100;

const uint16_t VOL_MAX	= 127;
const uint16_t VOL_MIN	= 1;
const uint16_t VOL_INI	= 80;

const uint32_t LOOP_MS	= 20;

const int32_t STEPS_MAX = 100;

#endif