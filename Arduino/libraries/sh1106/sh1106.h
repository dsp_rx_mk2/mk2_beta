#ifndef _SH1106_H_
#define _SH1106_H_

#include "inttypes.h"
#define NUM_W 8

void oled_init ();
void oled_sleep ();
void oled_awake ();
void oled_cmd (uint8_t* d, int8_t n);
void oled_data (uint8_t* d, int8_t n);
void oled_addr (uint8_t col, uint8_t row);
void oled_erase (uint8_t start_row = 0, uint8_t end_row = 7);
uint8_t oled_putc (const char c, uint8_t col, uint8_t row, uint8_t ww = NUM_W);
uint8_t oled_puts (const char* s, uint8_t x, uint8_t y, uint8_t ww = NUM_W);

#endif


