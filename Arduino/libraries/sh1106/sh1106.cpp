#include "font.h"
#include "sh1106.h"
#include <SPI.h>

#include "rx_pinout.h"

const uint8_t* font_bitmaps = (uint8_t*)microsoftSansSerif_11ptBitmaps;
const FONT_CHAR_INFO* font_descriptors = (FONT_CHAR_INFO*)microsoftSansSerif_11ptDescriptors;

#define CH_PUMP 1
#if (CH_PUMP)
  #define CONTRAST 0
#else
  #define CONTRAST 0xFF
#endif

//defaults correctly initialised by the HW reset are commented out
const uint8_t SH1106_init [] {
  //disable display (sleep)
  0xAE,

  //mux ratio
//  0xA8, 0x3F,

  //diplay offset
  0xD3, 0,

  //display start line
  0x40,

  //no remap
  0xA0,
//  0xA1,

  //COM output scan dir reverse
  0xC8,
//  0xC0,

  //COM pins (no idea, try)
  0xDA, 0x12,

  //contrast
//  0x81, 0xFF, //when not using the charge pump set to max
//  0x81, 0x00, //the charge pump is on keep it low
  0x81, CONTRAST,

  //display all on resume ?
//  0xA4,

  //normal/inverse to normal
//  0xA6,

  //oscillator
  0xD5, 0x80,

  //charge pump on
//  0x8D, 0x14,  //SSD1306
  0xAD, 0x8a|CH_PUMP,   //SH1106
  //charge pump off
//  0xAD, 0x8A,   //SH1106

  //SH1106 specific charge pump voltage (0x30..0x33)
  0x30,

  //display on
  0xAF
};

SPISettings oled_settings(4000000, MSBFIRST, SPI_MODE0);

void oled_cmd (uint8_t* b, int8_t n)
{
  SPI.beginTransaction (oled_settings);
  digitalWrite (OLED_CS, LOW);
  digitalWrite (OLED_DC, LOW);
  while (n--)
    SPI.transfer (*b++);
  digitalWrite (OLED_DC, HIGH);
  digitalWrite (OLED_CS, HIGH);
  SPI.endTransaction ();
}

void oled_data (uint8_t* b, int8_t n)
{
  SPI.beginTransaction (oled_settings);
  digitalWrite (OLED_CS, LOW);
  while (n--) {
    if (b)
      SPI.transfer (*b++);
    else
      SPI.transfer (0);
  }
  digitalWrite (OLED_CS, HIGH);
  SPI.endTransaction ();
}

void oled_addr (uint8_t col, uint8_t row)
{
  col+=2; //SH1106-specific, since it supports 132 (vs 128)
  uint8_t cmd[3] = {
    (uint8_t)(0xB0|(7-row)),
    (uint8_t)(col&0xf),
    (uint8_t)(0x10|(col>>4))
  };
  oled_cmd (cmd, 3);
}

void oled_init ()
{
  pinMode(OLED_DC, OUTPUT);
  pinMode(OLED_RST, OUTPUT);
  pinMode(OLED_CS, OUTPUT);
  digitalWrite (OLED_DC, LOW);
  digitalWrite (OLED_RST, LOW);
  digitalWrite (OLED_CS, HIGH);
  delay (10);
  digitalWrite (OLED_RST, HIGH);
  delay (10);
  oled_cmd((uint8_t*)SH1106_init, sizeof(SH1106_init));
}

void oled_sleep ()
{
  const uint8_t cmd[] = {
    0xAE,          //sleep mode
    0xAD, 0x8A,    //turn off the charge pump
  };
  oled_cmd ((uint8_t*)cmd, sizeof(cmd));
}

void oled_awake ()
{
  const uint8_t cmd[] = {
    0xAD, 0x8a|CH_PUMP, //turn on the charge pump
    0xAF,          //display on mode
  };
  oled_cmd ((uint8_t*)cmd, sizeof(cmd));
}

void oled_erase (uint8_t start, uint8_t end)
{
	int i;
	for (i = start; i <= end; i++)
	{
		oled_addr (0, i);
		oled_data (0, 128);
	}
}

uint8_t oled_putc (const char c, uint8_t col, uint8_t row, uint8_t ww)
{
  uint8_t d = c - ' ';
  uint8_t w = font_descriptors[d].w;
  uint16_t o = font_descriptors[d].o;
  int i;
  for (i = 0; i < 2; i++) {
    oled_addr (col, row++);
    if (c=='-'){
      oled_data(0, 1);
      if (i) col+=1;
    }
    else if (c=='+') {
      if (i) col-=2;
    }

    if (c==' '){
    	w = ww;
    	oled_data(0, w);
    }
    else if (c >= '0' && c <= '9') {
      //pad numbers on both sides to fixed width
      uint8_t p = (NUM_W-w)/2;
      oled_data (0, p);
      oled_data ((uint8_t*)&font_bitmaps[o], w);
      oled_data (0, NUM_W-w-p);
      if (i) w = NUM_W;
    }
    else {
      oled_data(0, 1);
      oled_data ((uint8_t*)&font_bitmaps[o], w);
      if (i) w++;
    }
    o+=w;
  }
  return col+w;
}  

uint8_t oled_puts (const char* s, uint8_t x, uint8_t y, uint8_t ww)
{
  while(*s)
    x = oled_putc (*s++, x, y, ww);
  return x;
}


