#include "buttons.h"

PBS b_array[N_B];
uint8_t b_lock = 0;

void b_init () {
	for (int i=0; i<N_B; i++) {
	  pinMode (B_PINS[i], INPUT_PULLUP);
		auto& b = b_array[i];
		b.state = ~0;
		b.tmr = 0;
	}
}

int b_update () {
	int b_cnt = 0;
	uint16_t states = 0xffff;
  for (int i=0; i<N_B; i++) {
  	auto bit = digitalRead(B_PINS[i]);
		auto& b = b_array[i];

		//count number of buttons curently pressed
    b_cnt += bit^1;

    //state transition timer 
    if ((bit ^ b.state) & 1)
			b.tmr = 0;
		else if (b.tmr < 0x7fff)
			b.tmr++;

    b.state = (b.state<<1)|bit;
    states &= b.state;
  }
	if (states & 2) b_lock = 0;
  return b_cnt;
}

bool b_s (const int& n) {
	return (b_array[n].state & 0x8007) == 0x8001;
}

bool b_l (const int& n) {
	return (b_array[n].state & 0xffff) == 0x0001;
}

bool b_p (const int& n, int t) {
	return !(b_array[n].state & 1) and b_array[n].tmr > t;
}

bool b_r (const int& n, int t) {
	return (b_array[n].state & 1) and b_array[n].tmr > t;
}