#ifndef _buttons_h_
#define _buttons_h_

#include "Arduino.h"
#include "rx_pinout.h"  

struct PBS {
    uint16_t state;
    int16_t tmr;
};

extern uint8_t b_lock;

const uint8_t ENC_LOCK = 2;
const uint8_t LPE_LOCK = 1;

void b_init (void);
int  b_update (void);

bool b_s (const int& n);
bool b_l (const int& n);
bool b_p (const int& n, int t = 15);
bool b_r (const int& n, int t = 15);

#endif
