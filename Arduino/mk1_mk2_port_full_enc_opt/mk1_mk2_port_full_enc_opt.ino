#include <Wire.h>
#include <SPI.h>
//#include <TimeLib.h>
//#include "Audio_new_modded.h"
#include "Audio_reduced.h"
#include "Audio_ext.h"
#include "sh1106.h"
#include "Si5338.h"
#include "rx_const.h"
#include "rx_pinout.h"  
#include "hilb.h"

// GUItool: begin automatically generated code
//AudioInputI2Sslave       adc;          //xy=84,123
//AudioOutputI2Sslave      dac;          //xy=536,115
AudioOutputI2S           dac;          //xy=536,115
AudioInputI2S            adc;          //xy=84,123

AudioFilterFIR           hilb_i;           //xy=233,85
AudioEffectDelay         dely_q;         //xy=233,168
AudioMixer4              mixer_l;         //xy=388,115
AudioMixer4              mixer_r;         //xy=388,115

AudioConnection          patchCord1(adc, 0, hilb_i, 0);
AudioConnection          patchCord2(adc, 1, dely_q, 0);

AudioConnection          patchCord3(hilb_i, 0, mixer_l, 0);
AudioConnection          patchCord4(dely_q, 0, mixer_l, 1);
AudioConnection          patchCord5(adc, 0, mixer_l, 2);

AudioConnection          patchCord6(hilb_i, 0, mixer_r, 0);
AudioConnection          patchCord7(dely_q, 0, mixer_r, 1);
AudioConnection          patchCord8(adc, 1, mixer_r, 2);

AudioConnection          patchCord9(mixer_l, 0, dac, 0);
AudioConnection          patchCord10(mixer_r, 0, dac, 1);

AudioAnalyzeCFFT256      fft_2;
AudioConnection          patchCord14(adc, 0, fft_2, 0);
AudioConnection          patchCord15(adc, 1, fft_2, 1);

AudioEffectMagnitude	 mag;
AudioConnection          patchCord16(adc, 0, mag, 0);
AudioConnection          patchCord17(adc, 1, mag, 1);
AudioConnection          patchCord18(mag, 0, mixer_l, 3);
AudioConnection          patchCord19(mag, 0, mixer_r, 3);

AudioControlSGTL5000     sgtl5000_1;     //xy=461,211
// GUItool: end automatically generated code

//program start time in millis
uint32_t ptime = 0;

//updated in the encoder ISR
volatile int32_t enc_cnt = 0;

//IF AGC, updated in the audio library ISR
volatile uint8_t agc_mode = 1;
volatile uint8_t agc = AGC_DEF;

//used for diagnostics, updated in the audio library ISR
volatile int32_t pdc = 0;
volatile int32_t avr = 0;
volatile int32_t avl = 0;

//analog bandwidth for SCAFs
uint16_t abw = 3800;

uint8_t mode = 3; //LSB
uint8_t g_mode = 0;
uint8_t cs_mode = 0;

Si5338* Si = 0;

int32_t freq = FRQ_INIT;
int16_t fstep = 1;

void set_mix ()
{
  float gain_i = 1.0;
  float gain_q = 1.0;
  float gain_a = 2.5;
  switch (mode) {
    case 0:
      mixer_l.gain(0, 0);
      mixer_l.gain(1, 0);
      mixer_l.gain(2, gain_i);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, 0);
      mixer_r.gain(1, 0);
      mixer_r.gain(2, gain_q);
      mixer_r.gain(3, 0);
      oled_puts((uint8_t*)"biq ", 0, 6);
      break;
    case 1:
      mixer_l.gain(0, gain_i);
      mixer_l.gain(1, -gain_q);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, gain_i);
      mixer_r.gain(1, gain_q);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, 0);
      oled_puts((uint8_t*)"bsb", 0, 6);
      break;
    case 2:
      mixer_l.gain(0, gain_i);
      mixer_l.gain(1, -gain_q);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, gain_i);
      mixer_r.gain(1, -gain_q);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, 0);
      oled_puts((uint8_t*)"usb", 0, 6);
      break;
    case 3:
      mixer_l.gain(0, gain_i);
      mixer_l.gain(1, gain_q);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, 0);
      mixer_r.gain(0, gain_i);
      mixer_r.gain(1, gain_q);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, 0);
      oled_puts((uint8_t*)"lsb ", 0, 6);
      break;
    case 4:
      mixer_l.gain(0, 0);
      mixer_l.gain(1, 0);
      mixer_l.gain(2, 0);
      mixer_l.gain(3, gain_a);
      mixer_r.gain(0, 0);
      mixer_r.gain(1, 0);
      mixer_r.gain(2, 0);
      mixer_r.gain(3, gain_a);
      oled_puts((uint8_t*)"am ", 0, 6);
      break;
  }
}

void set_fstep (uint16_t _fstep)
{
  fstep = _fstep;
  
  char buf[16];
  if (!g_mode) {
    sprintf(buf, "%3uHz", fstep);
    oled_puts((uint8_t*)buf, 87, 6);
  }
  else {
    sprintf(buf, "G%2uK", fstep/1000);
    oled_puts((uint8_t*)buf, 91, 6);
  }

  //step changed, need to reinit Si5338 clk0 multysynth
  double f = Si->clk0_set ((double)freq, (double)fstep);
  freq = floor (f + 0.5);
}

time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}

void setup() {
  //first intit button pullup, to avoid randomness
  pinMode (PB1, INPUT_PULLUP);
  pinMode (PB2, INPUT_PULLUP);
  pinMode (PB3, INPUT_PULLUP);

  Serial.begin(115200);
  Serial.println("--- reset ---");

/*
  setSyncProvider(getTeensy3Time);
  if (timeStatus()!= timeSet)
    Serial.println("Unable to sync with the RTC");
  else
    Serial.println("RTC has set the system time");
*/

  AudioMemory(20);

  hilb_i.begin(hilb, 128); 
  dely_q.delay_tap(0, 64);

  //Enable the audio shield, select input, and enable output
  /*  
  sampling rate changed to ~16k in the output_i2s.cpp only for 24MHz/48MHz/96MHz (F_PLL=96MHz)
  to avoid horrible MCLK jitter the MCLK_MULT must be 1 or 2
  using 2/47 multiplyer, the sampling rate is ~15.957k
  256-bin FFT is ~62.3338Hz
  */
  sgtl5000_1.enable();
  sgtl5000_1.inputSelect(AUDIO_INPUT_LINEIN);
  sgtl5000_1.lineInLevel(0);
  sgtl5000_1.volume(0.1);

  //built-in LED shares the audio adapter RX signal, can't be disabled :(
  
  //enable RF POWER
  pinMode (RF_PWR, OUTPUT);
  digitalWrite (RF_PWR, HIGH);
  delay (10);

  //generate clock for MAX7*
  pinMode (SCAF_CLK, OUTPUT);
  analogWriteFrequency(SCAF_CLK, abw*100);
  analogWrite(SCAF_CLK, 128);
  
  //encoder init
  pinMode (ENC_A, INPUT);   
  pinMode (ENC_B, INPUT);
  attachInterrupt (ENC_A, encoder, CHANGE);
  attachInterrupt (ENC_B, encoder, CHANGE);

  SPI.setMOSI(SDO);
  SPI.setSCK(SCLK);
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV4); //?

  //init display
  oled_init();
  oled_erase ();

  set_mix ();
  sgtl5000_1.volume(0.6);
  
  Wire.setClock(400000);
  delay (10);

  Si = new Si5338();
  set_fstep (fstep);

  ptime = millis();
}

//-------------------------------------------------------------------------------
void loop () {
  static uint8_t cnt = 0;
  static uint32_t ltime = millis();

  static uint16_t dabw = 0;
  static uint8_t dagc = 0;
  static int32_t dfreq = 0;
  static uint8_t wait = 200;
  static uint32_t ms_used = 0;
  static uint32_t wk_cnt = 0;
  static int32_t steps_avr = 0;

  //button mode control
  static uint16_t b1=0xffff;
  static uint16_t b2=0xffff;
  static uint16_t b3=0xffff;
  static uint8_t b1a = 1;
  static uint8_t b2a = 1;
  static uint8_t b3a = 1;

  static uint8_t enc_agc_arm = 1;
  static uint8_t enc_snap_arm = 0;
  static uint8_t enc_snap_arm_p = 0;

  static uint8_t mode_p = mode;

  while (ltime > millis()){
    asm("WFI");
    wk_cnt++;
  }

  //capture buttons
  b1 = (b1<<1)|digitalRead(PB1);
  b2 = (b2<<1)|digitalRead(PB3);
  b3 = (b3<<1)|digitalRead(PB2);

  //copy from mk1 code
  //on b1 release if no encoder action
  if (b1a == 1 && (b2&b3&1)) {
    if ((b1&0x8007)==0x8001) {
      //short action
      if (cs_mode) {
        if (++mode > 3) mode = 1;
      }
      else {
        if (++mode > 4) mode = 1;
      }
      set_mix();
    }
    if ((b1&0x8007)==1) {
      //long action
      if (mode < 4 && cs_mode == 0 && fstep == 1) {
          g_mode = 0;
          cs_mode = 1;
      }
      else
          cs_mode = 0;
      enc_snap_arm = cs_mode;
      
      if (cs_mode)
        oled_puts((uint8_t*)"cs", 27, 6);
      else
        oled_puts((uint8_t*)"   ", 27, 6);
    }
  }
  //on b2 release if no encoder action
  if (b2a == 1 && (b1&b3&1)) {
    if ((b2&0x8007)==0x8001) {
      //short action
      g_mode ^= 1;
      if (!g_mode)
        set_fstep (1);
      else if (dfreq > FRQ_MW)
        set_fstep (5000);
      else
        set_fstep (10000);
    }
  }

  if (b3a == 1 && (b1&b2&1)) {
    if ((b3&0x8007)==1) {
      //long action
      agc_mode = 2;
    }
  }

  //for now reset release action on double press
  if (b1 == 0 && b2 == 0) b1a = 0, b2a = 0, b3a = 0;

  //re-arm release action
  if((b1&1) == 1) b1a = 1;
  if((b2&1) == 1) b2a = 1;
  if((b3&1) == 1) b3a = 1;

  //various debug reporting
  const uint8_t REPT = 64;
  if (cnt%REPT == 0){
#if defined(__MK20DX256__)
    Serial.println("---T3.2---");
#elif defined(__MK64FX512__)
    Serial.println("---T3.5---");
#elif defined(__MK66FX1M0__)
    Serial.println("---T3.6---");
#endif
   
#if(0)    
    Serial.println(F_CPU);
    Serial.println(F_PLL);
    Serial.println(AUDIO_SAMPLE_RATE);
#endif

#if (1)
    Serial.print("LTU ");
    Serial.print((float)ms_used/REPT);
    Serial.println("ms");
    ms_used = 0;
    Serial.print("WKC ");
    Serial.println((float)wk_cnt/REPT);
    wk_cnt = 0;
    Serial.print("STA ");
    Serial.println(steps_avr);
#endif 

#if(0)    
    if (fft_2.max_cnt == 0) {
      Serial.println(fft_2.freq_est);
    }
#endif    

#if(1)
    Serial.print("APU ");
    Serial.print(AudioProcessorUsageMax()*16e3/AUDIO_SAMPLE_RATE);
    Serial.println("%");
    AudioProcessorUsageMaxReset();
    Serial.print("AMU ");
    Serial.println(AudioMemoryUsageMax());
    AudioMemoryUsageMaxReset();
#endif  

#if(0)
    Serial.print(avr>>16);
    Serial.print(",");
    Serial.print(avl>>16);
    Serial.print(",");
    Serial.println(pdc>>12);
#endif  
  }
  
  //snap to carrier
  if (enc_snap_arm && fft_2.max_cnt == 0 && millis()-ptime > 1000) {
    enc_snap_arm = 0;
    if (agc_mode) agc_mode = 2;
    freq += (int32_t)(fft_2.freq_est);  
  }

	//temporarily switch to AM mode during snap search
	if (enc_snap_arm == 1 && enc_snap_arm_p == 0) {
		mode_p = mode;
		mode = 4;
		set_mix ();
	}
	else if (enc_snap_arm == 0 && enc_snap_arm_p == 1) {
		mode = mode_p;
		set_mix ();
	}
	enc_snap_arm_p = enc_snap_arm;

  if (wait)
    wait--;
  else if (enc_agc_arm && millis()-ptime > 1000) {
    enc_agc_arm = 0;
    if (agc_mode) agc_mode = 2;
  }

  //process encoder
  uint8_t slow = 3;
  if (g_mode && (b1&b2&b3&1)) slow = 5;
  int32_t steps = enc_cnt>>slow;
  enc_cnt -= steps<<slow;

  //add one step hysteresis
  static int32_t psteps = 0;
  if (steps*psteps == -1) psteps = steps, steps = 0;
  else if (steps) psteps = steps;

  const uint8_t ALPHA = 6;
  steps_avr += ((steps*steps<<ALPHA)-steps_avr)>>ALPHA;

  if (steps && (b1&b2&b3&1)) {
    if (!g_mode && cs_mode)
      enc_snap_arm = 1;
    if (g_mode && steps_avr > (10<<ALPHA))
      enc_agc_arm = 1;

    //update frequency
    steps *= abs(steps);
    steps = min(STEPS_MAX, max(-STEPS_MAX, steps));
    freq += steps*fstep;
    freq = min(FRQ_MAX, max(FRQ_MIN, freq));
  }
  else if (steps > 0 && (b2&b3&1) && b1==0 && abw < 6000)
    abw += 100;
  else if (steps < 0 && (b2&b3&1) && b1==0 && abw > 1000)
    abw -= 100;
  else if (steps > 0 && (b1&b2&1) && b3==0 && agc < AGC_MAX)
    agc++, agc_mode = 0;
  else if (steps < 0 && (b1&b2&1) && b3==0 && agc > AGC_MIN)
    agc--, agc_mode = 0;

  if (steps) {
    //reset long press arming, encoder has moved
    if ((b1&1) == 0) b1a = 0;
    if ((b2&1) == 0) b2a = 0;
    if ((b3&1) == 0) b3a = 0;
  }
  
  if (!g_mode) {
    //auto update fstep if not in the grid mode
    if (steps_avr > (50<<ALPHA) && fstep < 100)
      steps_avr = (30<<ALPHA), set_fstep(10*fstep);
    if (steps_avr < (5<<ALPHA) && fstep > 1)
      steps_avr = (30<<ALPHA), set_fstep(fstep/10);
  }
  else {
    //switch fstep when crossing the upper limit of the MW band
    if (dfreq > FRQ_MW && freq <= FRQ_MW)
      set_fstep(10000);
    else if (dfreq < FRQ_MW && freq >= FRQ_MW)
      set_fstep(5000);
  }

  //service and display updates
  char buf [32];

  if (freq != dfreq) {
    dfreq = freq;
    Si->fsteps (steps);
    //display freq
    sprintf (buf, "%6ld.%03ld kHz", (dfreq-FRQ_IF)/1000, (dfreq-FRQ_IF)%1000);
    oled_puts((uint8_t*)buf, 0, 0);
  }
  
  if (agc != dagc) {
    dagc = agc;
    analogWrite(AGC_OUT, agc);
    //display agc value
    sprintf (buf, "%3d", dagc);
    oled_puts((uint8_t*)buf, 50, 6);
  }

  if (abw != dabw) {
    dabw = abw;
    analogWriteFrequency(SCAF_CLK, abw*100);
    //display audio bandwidth
    sprintf (buf, "%4d  Hz", dabw);
    oled_puts((uint8_t*)buf, 42, 2);
  }

/*
  if (psec != second()) {
    psec = second();
    //display time and date
    sprintf (buf, "%02d:%02d:%02d utc", (hour()+5)%24, minute(), second());
    oled_puts((uint8_t*)buf, 10, 4);
    sprintf (buf, "%02d/%02d/%02d", month(), day(), year()-2000);
    oled_puts((uint8_t*)buf, 66, 4);
  }
*/

  if(cnt%2 == 0) {
    //display graph
    uint8_t r1[128], r2[128];
    for (int i = 0; i < 128; i++) {
      int16_t ix = 192-i;
      uint16_t dot = 1<<(fft_2.l2mag[ix]>>4);
      if (ix == fft_2.max_i && enc_snap_arm) dot ^= 0xffff;
      r1[i] = dot>>8;
      r2[i] = dot;
    }
    oled_addr (0, 4);
    oled_data (r1, 128);
    oled_addr (0, 5);
    oled_data (r2, 128);
  }

  cnt++;
  ms_used += millis()-ltime;
  ltime += LOOP_MS;
}

//ISR function ------------------------------------------------
void encoder ()
{
  static uint8_t pstate = 0;

  //process quadrature state at any speed
  uint8_t state = digitalRead(ENC_A);
  if (digitalRead(ENC_B)) state ^= 3;
  if (state == ((pstate + 1)&3))
    enc_cnt++;
  if (state == ((pstate - 1)&3))
    enc_cnt--;
  pstate = state;
}

