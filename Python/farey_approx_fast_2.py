import math

#in C/C++ use double precision for all floats
def farey_approx (v, prec, d_max):
    vi = math.floor(v)
    vf = v - vi
    xn,xd,yn,yd = 0,1,1,1   # initial search interval 0/1..1/1

    n = m = 0;
    while (1):
        #calculate the number of steps in one directions until error sign changes
        k = math.ceil((yn - vf*yd)/(vf*xd - xn));
        if (k <= 0): print ("warning: k %d"%k);
        d_max -= k*xd;
        if (d_max < 0):  #do not update x's
            k += d_max//xd;
            yn += k*xn; yd += k*xd;
        else:
            yn += k*xn; yd += k*xd;
            xn = yn - xn; xd = yd - xd;
        n += k; m += 1;

        xerr = xn/xd - vf;
        yerr = yn/yd - vf;
        print (xn, xd, yn, yd);
        print ("%.2e, %.2e"%(xerr, yerr))

        if (d_max < 0):
            print("return on denom exceeding max value")
            if (abs(xerr) < abs(yerr)):
                return (vi, xn, xd), xerr, n;
            return (vi, yn, yd), yerr, n, m;

        #xd < yd always, so if we pass prec check - return
        if (abs(xerr) <= prec):
            print("return on passed precision check")
            return (vi, xn, xd), xerr, n, m;

        if (abs(yerr) <= prec):
            print("return on passed precision check")
            return (vi, yn, yd), yerr, n, m;
        

v = math.pi
#v = math.e
#v = math.sqrt(2)
#v = 1e-4 + 7e-6;

print ("fast_2\n\n");
r = farey_approx (v, 1e-10, 1<<17);
print ("\nfloat %.10e"%v);
print ("fract %d %d/%d"%(r[0]))
print ("approx error %.2e"%(r[1]))
print ("N steps in the tree", r[2])
print ("M iterations", r[3]);
